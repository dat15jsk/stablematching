package stable_matching;

import java.util.Stack;

public abstract class Person {
    
    protected String name;
    protected int id;
    protected Stack<Integer> preference;
    
    public String getName() {
        return name;
    }
    
    public int getId() {
        return id;
    }
    
    public void addPreference(Integer id) {
        preference.push(id);
    }
    
    public Stack<Integer> getPreference() {
        return preference;
    }

}
