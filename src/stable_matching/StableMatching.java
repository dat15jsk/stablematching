package stable_matching;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class StableMatching {

    private ArrayList<Man> men;
    private HashMap<Integer, Woman> women;

    public static void main(String[] args) throws NumberFormatException, IOException {
        StableMatching sm = new StableMatching(new File(args[0]));
        OutputWriter.write(sm.stableMatching());
    }

    public StableMatching(File input) throws NumberFormatException, IOException {
        men = InputReader.readMen(input);
        women = InputReader.readWomen(input);
    }

    public HashMap<Woman, Man> stableMatching() {
        HashMap<Woman, Man> s = new HashMap<>();
        while (!men.isEmpty()) {
            Man m = men.get(0);
            if (!m.getPreference().isEmpty()) {
                Woman w = women.get(m.getPreference().pop());
                if (!s.containsKey(w)) {
                    s.put(w, m);
                    men.remove(0);
                } else if (w.prefers(s.get(w).getId(), m.getId())) {
                    men.add(s.get(w));
                    s.remove(w, s.get(w));
                    s.put(w, m);
                    men.remove(0);
                }
            } else {
                men.remove(0);
            }
        }
        return s;
    }
}
