package stable_matching;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

public class OutputWriter {
    
    public static void write(HashMap<Woman, Man> matching) throws IOException {
        File output = new File("output.txt");
        FileWriter writer = new FileWriter(output);
        for (Entry<Woman, Man> e: matching.entrySet()) {
            System.out.println(e.getValue().getName() + " -- " + e.getKey().getName() + "\n");
            writer.write(e.getValue().getName() + " -- " + e.getKey().getName() + "\n");
        }
        writer.close();
    }

}
