package stable_matching;

import java.util.Stack;

public class Woman extends Person {
    
    public Woman(String name) {
        super.name = name;
        super.id = id;
        super.preference = new Stack<>();
    }
    
    public void dump() {
        
    }
    
    public boolean prefers(int fiance, int poacher) {
        @SuppressWarnings("unchecked")
        Stack<Integer> pref = (Stack<Integer>) preference.clone();
        while(!pref.isEmpty()) {
            int m = pref.pop();
            if(m == fiance) {
                return false;
            } else if(m == poacher) {
                return true;
            }
        }
        return false;
    }

}
