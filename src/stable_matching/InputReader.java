package stable_matching;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class InputReader {

    public static ArrayList<Man> readMen(File input) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(input));
        String comment = "#";
        while (comment.charAt(0) == '#') {
            comment = reader.readLine();
        }
        int size = Integer.parseInt(comment.split("=")[1]);
        ArrayList<Man> men = new ArrayList<>();
        for (int i = 1; i < size * 2; i += 2) {
            String name = reader.readLine().split(" ")[1];
            men.add(new Man(name, i));
            reader.readLine();
        }
        reader.readLine();
        for (Man m : men) {
            String[] line = reader.readLine().split(" ");
            for (int i = line.length - 1; i > 0; i--) {
                m.addPreference(Integer.parseInt(line[i]));          
            }
            reader.readLine();
        }
        reader.close();
        return men;
    }

    public static HashMap<Integer, Woman> readWomen(File input) throws NumberFormatException, IOException {
        BufferedReader reader = new BufferedReader(new FileReader(input));
        String comment = "#";
        while (comment.charAt(0) == '#') {
            comment = reader.readLine();
        }
        int size = Integer.parseInt(comment.split("=")[1]);
        HashMap<Integer, Woman> women = new HashMap<>();
        for (int i = 2; i < size * 2 + 1; i += 2) {
            reader.readLine();
            String name = reader.readLine().split(" ")[1];
            women.put(i, new Woman(name));
        }
        reader.readLine();
        for (Integer i : women.keySet()) {
            reader.readLine();
            String[] line = reader.readLine().split(" ");
            for (int j = line.length - 1; j > 0; j--)
                women.get(i).addPreference(Integer.parseInt(line[j]));
        }
        reader.close();
        return women;
    }

}
