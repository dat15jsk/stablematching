package test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import stable_matching.Man;
import stable_matching.OutputWriter;
import stable_matching.StableMatching;
import stable_matching.Woman;

public class InputReaderTest {
    
    HashMap<Woman, Man> matching;
    
    @Before
    public void setUp() throws NumberFormatException, IOException {
        StableMatching sm = new StableMatching(new File("data\\sm-bbt-in.txt"));
        matching = sm.stableMatching();
    }
    
    @Test
    public void testInput() {
        
    }

}
